class Api::V1::PostsController < Api::V1::BaseController
  api :GET, "/posts", "Posts List"
  def index
    @sample = { name: "post 1 should get this from db and loop this result" }
    # render json: { result: [{ name: "post 1"}] }, status: 200
  end
end
